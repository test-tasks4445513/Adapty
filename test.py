import pytest
import requests

# Configuration: Base URL of the API and the secret token for authentication
BASE_URL = "https://api.adapty.io/api/v1"
SECRET_TOKEN = "secret_live_BEHrYLTr.ce5zuDEWz06lFRNiaJC8mrLtL8fUwswD"  # Replace with your actual secret token

# Setup: Common headers for all requests, including the Authorization header with the API key
headers = {
    "Authorization": f"Api-Key {SECRET_TOKEN}",
    "Content-Type": "application/json",
}

def generate_custom_attributes(num_attributes, base_value="valid", exceed_length=False):
    """
    Generates a dictionary of custom attributes for testing.
    
    Parameters:
    - num_attributes: Number of attributes to generate.
    - base_value: The base value to use for generating attribute values. Its type influences the type of attributes generated.
    - exceed_length: A flag to indicate whether the generated attributes should exceed API constraints, e.g., key length.
    
    Returns:
    - A dictionary of custom attributes.
    """
    attributes = {}
    for i in range(num_attributes):
        # Generate a key name. If exceed_length is True, make the key longer than allowed.
        key = f"key_{i}" + ("_extra_long" * 5 if exceed_length else "")
        # Limit the key to 30 characters unless testing for exceed_length scenario
        key = key[:30] if not exceed_length else key

        # Prepare the value based on the base_value parameter and exceed_length flag
        value = (base_value * 10 if isinstance(base_value, str) else base_value) if exceed_length else base_value
        # Ensure string values do not exceed 30 characters, unless testing for length constraint violations
        value = value[:30] if isinstance(value, str) and not exceed_length else value
        
        attributes[key] = value
    return attributes

# Define parameterized test cases to cover various scenarios
@pytest.mark.parametrize("custom_attributes, expected_status", [
    # Valid custom attributes within constraints
    (generate_custom_attributes(10, "valid_string"), 200),
    (generate_custom_attributes(10, 123.45), 200),
    (generate_custom_attributes(10, True), 200),
    
    # Exceeding the limit of custom attributes
    (generate_custom_attributes(11, "exceed_limit"), 400),
    
    # Testing maximum allowed lengths
    ({"max_length_key": "a" * 30}, 200),
    ({"max_length_value": "v" * 30}, 200),
    
    # Keys exceeding maximum length
    (generate_custom_attributes(1, "valid", exceed_length=True), 400),
    
    # Values exceeding maximum length
    ({"valid_key": "a" * 31}, 400),
    
    # Invalid characters in key
    (generate_custom_attributes(1, "valid", invalid_key=True), 400),
    
    # Deleting attributes
    ({"delete_this": None}, 200),
    ({"delete_this": ""}, 200),
    
    # Additional tests
    ({"": "empty_key"}, 400),  # Empty key
    ({"null_value": None}, 200),  # Null value for valid key
    ({"valid_key_with_under_score": "valid_value"}, 200),  # Valid key with underscore
    ({"keyWithDot": "value.with.dot"}, 200),  # Key and value with dot
])


def test_custom_attributes_validation(custom_attributes, expected_status, expected_attributes):
    profile_id_or_customer_user_id = "example_profile_id"
    endpoint = f"/profiles/{profile_id_or_customer_user_id}/"
    
    # PATCH request to update custom attributes
    patch_response = requests.patch(f"{BASE_URL}{endpoint}", json={"custom_attributes": custom_attributes}, headers=headers)
    assert patch_response.status_code == expected_status, f"PATCH expected status {expected_status}, got {patch_response.status_code}"

    # If the test case is expected to succeed, verify the changes were applied
    if expected_status == 200:
        # GET request to fetch and verify the updated profile state
        get_response = requests.get(f"{BASE_URL}{endpoint}", headers=headers)
        assert get_response.status_code == 200, "Failed to fetch the profile for verification."
        
        profile_data = get_response.json()
        for key, expected_value in expected_attributes.items():
            actual_value = profile_data.get("custom_attributes", {}).get(key)
            assert actual_value == expected_value, f"Attribute {key} was expected to be {expected_value}, but found {actual_value} instead."

# NB! Replace "example_profile_id" and "secret_live_BEHrYLTr.ce5zuDEWz06lFRNiaJC8mrLtL8fUwswD" 
# with actual values before running the tests.
